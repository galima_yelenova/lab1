﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBigNumber
{
    struct BigNumber
    {
        public int[] a;
        public int size;
        public BigNumber(string s)
        {
            a = new int[1000];
            size = s.Length - 1;
            for (int i = size, j = 0; size >= 0; size--, j++)
            {
                a[j] = a[i] - '0';
            } 
        }
        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < size; i++)
            {
                s += a[i].ToString();
            }
                return s.ToString();
        }
            
        }
    
    class Program
    {
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            Console.WriteLine(s1);
            Console.ReadKey(); 
        }
    }
}
