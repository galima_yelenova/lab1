﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W2operator_substr
{
    struct DNumber
    {
        int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }
        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();
        }
        public void normalize()
        {
            int x = Math.Abs(a), y = Math.Abs(b);
            while (x > 0 && y > 0)
            {
                if (x > y)
                {
                    x %= y;
                }
                else
                    y %= x;
            }
            x += y;
            a /= x;
            b /= x;
        }
        public static DNumber operator -(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.b *= arg2.b;
            arg1.a -= arg2.a;
            arg1.normalize();
            return arg1;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DNumber x = new DNumber(-4, 5);
            DNumber y = new DNumber(-8, 9);
            DNumber z = x - y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
