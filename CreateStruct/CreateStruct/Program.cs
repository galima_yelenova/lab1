﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateStruct
{
    struct Person
    {
        public enum Genders : int { Male, Female };
        public string FirstName;
        public string LastName;
        public int age;
        public Genders gender;

        public Person (string _FirstName, string _LastName, int _age, Genders _gender)
        {
            FirstName = _FirstName;
            LastName = _LastName;
            age = _age;
            gender = _gender;
        }
        public override string ToString()
        {
            return FirstName.ToString() + " " + LastName.ToString() + " " + gender.ToString() + " " + age.ToString();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person a = new Person("Alan", "Miln", 18, Person.Genders.Male);
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
