﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3._1
{
    class Program
    {
        class Person
        {
            public enum Genders : int { Male, Female };
            public string FirstName;
            public string LastName;
            public int age;
            public Genders gender;
            
            public Person(string _FirstName, string _LastName, int _age, Genders _gender)
            {
                FirstName = _FirstName;
                LastName = _LastName;
                age = _age;
                gender = _gender;
            }
            public override string ToString()
            {
                return FirstName.ToString() + " " + LastName.ToString() + " " + gender.ToString() + " " + age.ToString();
            }
        }
        class Manager : Person
        {
            public string phoneNumber;
            public string officeLocation;
            public Manager (string _FirstName, string _LastName, int _age, Genders _gender, string _phoneNumber, string _officeLocation)
                : base(_FirstName, _LastName, _age, _gender)
            {
                phoneNumber = _phoneNumber;
                officeLocation = _officeLocation;
            }
            public override string ToString()
            {
                return base.ToString() + " " + phoneNumber.ToString() + " " + officeLocation.ToString();
            }
        }

        static void Main(string[] args)
        {
            Manager p = new Manager ("Tony", "Allen", 32, Person.Genders.Male, "8777777777", "Tole bi street");
            Console.WriteLine(p);
            Console.ReadKey();

        }
    }
}
